'use strict'
module.exports = {
  NODE_ENV: '"production"',
  URI: '"https://digital.prowrestlingscorecards.com:3000"',
  ONESIGNAL_APPID: '"5cd6b04b-1b60-4738-82b3-6b5ffd7bfe68"',
}
