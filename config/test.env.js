'use strict'
const merge = require('webpack-merge')
const devEnv = require('./dev.env')

module.exports = merge(devEnv, {
  NODE_ENV: '"testing"',
  URI: '"http://digitest.prowrestlingscorecards.com:3001"',
  ONESIGNAL_APPID: '"6c5db8eb-be69-49e0-adb1-e94226b82b99"',
})
