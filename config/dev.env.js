'use strict'
const merge = require('webpack-merge')
const prodEnv = require('./prod.env')

module.exports = merge(prodEnv, {
  NODE_ENV: '"development"',
  URI: '"http://192.168.0.27:3000"',
  ONESIGNAL_APPID: '"6c5db8eb-be69-49e0-adb1-e94226b82b99"',
})
