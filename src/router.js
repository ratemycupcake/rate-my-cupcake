import Vue from 'vue';
import Router from 'vue-router';
import Cookies from 'js-cookie';
import store from './store';
import api from './lib/api';
import Home from './views/Home.vue';
import Map from './views/Map.vue';
import Cake from './views/Cake.vue';
import Account from './views/Account.vue';
import SignIn from './views/SignIn.vue';
import SignUp from './views/SignUp.vue';

Vue.use(Router);

const router = new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home,
    },
    {
      path: '/about',
      name: 'about',
      component: () => import('./views/About.vue'),
    },
    {
      path: '/map/:id?',
      name: 'map',
      component: Map,
    },
    {
      path: '/cakes',
      name: 'cakes',
      component: () => import('./views/Cakes.vue'),
    },
    {
      path: '/cakes/:id',
      name: 'cake',
      component: Cake,
    },
    {
      path: '/account',
      name: 'account',
      component: Account,
    },
    {
      path: '/login',
      name: 'login',
      component: SignIn,
    },
    {
      path: '/signup',
      name: 'signup',
      component: SignUp,
    },
  ],
});

router.beforeEach((to, from, next) => {
  const jwt = Cookies.get('rmc_jwt');
  if (!store.getters.loggedIn && jwt && jwt.length > 0) {
    api.autoLogin(jwt).then(() => {
      next();
    });
  }
  next();
});


export default router;
