// eslint-disable-next-line
import 'material-design-icons-iconfont/dist/material-design-icons.css';
import Vue from 'vue';
import Vuex from 'vuex';
import VueLayers from 'vuelayers';
import App from './App.vue';
import router from './router';
import vuetify from './plugins/vuetify';
import store from './store';

// eslint-disable-next-line
const request = require('request');

Vue.config.productionTip = false;

Vue.use(Vuex);
Vue.use(VueLayers);

new Vue({
  router,
  vuetify,
  store,
  icons: {
    iconfont: 'md',
  },
  render: h => h(App),
}).$mount('#app');
