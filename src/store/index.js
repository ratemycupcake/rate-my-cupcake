import Vue from 'vue';
import Vuex from 'vuex';
import Cookies from 'js-cookie';

Vue.use(Vuex);

const store = new Vuex.Store({
  state: {
    user: null,
  },
  mutations: {
    login(state, payload) {
      state.user = payload;
    },
  },
  actions: {
    login({ commit }, payload) {
      let user = payload.safeUser;
      if (!user) {
        user = payload;
      }
      commit('login', user);
    },
    logout({ commit }) {
      Cookies.remove('rmc_jwt');
      commit('login', null);
    },
  },
  getters: {
    loggedIn(state) {
      return state.user !== null && state.user !== undefined;
    },
    avatar(state) {
      return state.user.avatar;
    },
  },
});

export default store;
