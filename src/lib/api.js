import store from '../store/index';
// eslint-disable-next-line
const request = require('request');

const apiUrl = 'http:\\\\192.168.1.100:3000';

export default {
  autoLogin(jwt) {
    return new Promise((resolve) => {
      request.post(
        {
          uri: `${apiUrl}/users/auto_login`,
          body: { jwt },
          json: true,
        },
        (err, res) => {
          if (res.body.id) {
            store.dispatch('login', res.body);
            return resolve();
          }
          return resolve();
        },
      );
    });
  },
  changePassword(user, password) {
    return new Promise((resolve) => {
      request.post(
        {
          uri: `${apiUrl}/users/password`,
          body: { user: user.id, password },
          json: true,
        },
        (err, res) => {
          if (res.body.id) {
            return resolve();
          }
          return resolve();
        },
      );
    });
  },
  loadAllCakes() {
    return new Promise((resolve, reject) => {
      let cakes = [];
      request.get(
        {
          uri: `${apiUrl}/cakes`,
        },
        (err, res) => {
          if (res.statusCode === 200) {
            const response = JSON.parse(res.body);
            cakes = response;
            return resolve(cakes);
          }
          return reject();
        },
      );
    });
  },
  loadCake(id) {
    return new Promise((resolve, reject) => {
      let userId;
      try {
        userId = store.state.user.id;
      } catch {
        userId = undefined;
      }
      request.post(
        {
          uri: `${apiUrl}/cakes/${id}`,
          body: {
            user_id: userId,
          },
          json: true,
        },
        (err, res) => {
          const cake = res.body;
          if (cake) {
            return resolve(cake);
          }
          return reject(err, cake);
        },
      );
    });
  },
  loadRecentCakes(recent, topNear) {
    let cakes = [];
    if (!topNear) {
      // eslint-disable-next-line no-param-reassign
      topNear = false;
    }
    if (!recent) {
      // eslint-disable-next-line no-param-reassign
      recent = false;
    }
    return new Promise((resolve, reject) => {
      request.get(
        {
          uri: `${apiUrl}/cakes${topNear ? '/top' : ''}`,
        },
        (err, res) => {
          if (res.statusCode === 200) {
            const response = JSON.parse(res.body);
            cakes = response;
            if (recent) {
              cakes = cakes.reverse().splice(0, 10);
            }
            return resolve(cakes);
          }
          return reject();
        },
      );
    });
  },
  loadCakeLocations() {
    return new Promise((resolve, reject) => {
      request.get(`${apiUrl}/cakes/locations`,
        (err, res) => {
          if (res.statusCode === 200) {
            const cakes = JSON.parse(res.body);
            return resolve(cakes);
          }
          return reject(err, res);
        });
    });
  },
  rateCake(cake, rating) {
    return new Promise((resolve, reject) => {
      request.post(
        {
          uri: `${apiUrl}/cakes/${cake.id}/rate`,
          body: rating,
          json: true,
        },
        (err) => {
          if (err) {
            return reject(err);
          }
          return resolve();
        },
      );
    });
  },
  favouriteCake(cake) {
    return new Promise((resolve, reject) => {
      request.post(
        {
          uri: `${apiUrl}/cakes/${cake.id}/favourite`,
          body: {
            user_id: store.state.user.id,
          },
          json: true,
        },
        (err) => {
          if (err) {
            return reject(err);
          }
          return resolve();
        },
      );
    });
  },
  loadProfile(user) {
    const { id } = user;
    return new Promise((resolve, reject) => {
      request.post(
        {
          uri: `${apiUrl}/users/profile`,
          body: {
            user_id: id,
          },
          json: true,
        },
        (err, profile) => {
          if (err) {
            return reject(err);
          }
          return resolve(profile.body);
        },
      );
    });
  },
  addCake(cake) {
    // eslint-disable-next-line no-param-reassign
    cake.user_id = store.state.user.id;
    return new Promise((resolve, reject) => {
      request.post(
        {
          uri: `${apiUrl}/cakes`,
          body: cake,
          json: true,
        },
        (err, profile) => {
          if (err) {
            return reject(err);
          }
          return resolve(profile.body);
        },
      );
    });
  },
};
